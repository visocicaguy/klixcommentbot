<?php

$fp = fopen("klix/klix.txt", "wb");
if( $fp == false ){
    die('Cannot create a file, check permissions');
}

processPage(file_get_contents('https://www.klix.ba/najnovije'), $fp, 1);
processPage(file_get_contents('https://www.klix.ba/najnovije/str2'), $fp, 2);

fclose($fp);

function processPage($page, $file, $prepend) {
    $articles = null;
    preg_match_all('/\<article class="(.*?)\<\/article>/', $page, $articles);

    foreach ($articles[0] as $article) {
        $article = preg_replace("/<img[^>]+\>/i", "", $article);
        $title = null;
        preg_match('/<h1(.*?)>(.*?)<\/h1>/', $article, $title);
        $url = null;
        preg_match('/href="(.*?)"/', $article, $url);
        $comments = null;
        preg_match('/\<span class="comments">(.*)<\/span>/', $article, $comments);
        if (strpos($url[1], 'humanitarne-akcije') === false &&
            strpos($url[1], 'crna-hronika') === false &&
            strpos($url[1], 'sport') === false &&
            strpos($url[1], 'magazin') === false &&
            strpos($url[1], 'lifestyle') === false &&
            strpos($url[1], 'scitech') === false &&
            strpos($url[1], 'auto') === false &&
            strpos($title[2], 'uhapšen') === false &&
            strpos($title[2], 'poginuo') === false &&
            strpos($title[2], 'poginula') === false &&
            strpos($title[2], 'preminuo') === false &&
            strpos($title[2], 'silovao') === false &&
            strpos($title[2], 'silovan') === false &&
            strpos($title[2], 'ubio') === false &&
            strpos($title[2], 'ubijen') === false &&
            strpos($title[2], 'Dragičević') === false &&
            strpos($title[2], 'Dženan Memić') === false &&
            strpos($title[2], 'Sefić') === false &&
            strpos($title[2], 'Agić') === false &&
            strpos($title[2], 'Malkoč') === false &&
            strpos($title[2], 'lakše povrede') === false &&
            strpos($title[2], 'teže povrede') === false &&
            strpos($title[2], 'povrijeđen') === false &&
            strpos($title[2], 'sudar') === false &&
            strpos(strip_tags($title[2]), 'Promo /') !== 0) {

            fwrite($file, $prepend . ';' . trim(strip_tags($comments[1])) . ';https://klix.ba' . $url[1] . '#komentari' . PHP_EOL);
        }
    }
}
