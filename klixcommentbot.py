import random, math
from datetime import datetime

loopctl = True
def handler(event):
    global loopctl
    loopctl = False
Env.addHotkey(Key.END, KeyModifier.CTRL, handler)

brojKomentara = 500
vrijemepauze = 60
users1 = ["ijasamiskra1","budiiskrapromjene","samoiskreno123"]
users2 = ["TaksistaZe","EsmeraldaMala","Pekar1980"]
users3 = ["NepozeljanTip","NeozenjenAzgodan47","Nisamjanikako"]
users4 = ["pijanaulica","LjutiFrajer","UpitnikItekako"]
users5 = ["Pandora71","pusenjejestetno","brckozauvijek"]
users6 = ["MehmedaSpahe14","Alipasina41","Tesanjska24a"]
users7 = ["Zvucnik666","UzvIcnikIII","Hamozbrkovima"]
users8 = ["theGoldenLilly86","FikretinMuz","desetupola"]
users9 = ["semperdicverum","vacuamduccriminevita","Hamajlija123"]
users10 = ["ferpatienteronus","facsapienteropus","Hamajlija123"]
flag = "🇧🇦"
symbols = ["👍","💫","💥","⚜","✨","🎯"]
hashtags = ["#ijasamiskra", "#mislislisvojomglavom","#pravipredsjednik","#novaBiH"]
#poruka = ["I neka je izasao iz emisije na FTV-u i sasuo im istinu u lice!! Momenat iz emisije na FTV-u: https://bit.ly/2R4aoGC","Sada imate za koga da glasate // Hadzikadicev predizborni spot: https://bit.ly/2DvUTEN","#Hadzikadic ne finansira kampanju od budžetskog (vašeg) novca, dok drugi kandidati koriste vaš novac kako bi vas gledali sa visine. #MH kampanju finansira samostalno.","Neki kažu: 'Ne želim Fahru, pa ću glasati za Šefika.' Drugi opet: 'Neću Čovića, pa ću glasati za Komšića.' DOSTA!!! Kada ćemo prestati da glasamo PROTIV i početi da glasamo ZA? GLASAJMO NAPOKON ZA NAJSPOSOBNIJEG!","Neki kažu: 'Ne želim Fahru, pa ću glasati za Šefika.' Drugi opet: 'Neću Čovića, pa ću glasati za Komšića.' DOSTA!!! Kada ćemo prestati da glasamo PROTIV i početi da glasamo ZA? GLASAJMO NAPOKON ZA NAJSPOSOBNIJEG!","Neki kažu: 'Ne želim Fahru, pa ću glasati za Šefika.' Drugi opet: 'Neću Čovića, pa ću glasati za Komšića.' DOSTA!!! Kada ćemo prestati da glasamo PROTIV i početi da glasamo ZA? GLASAJMO NAPOKON ZA NAJSPOSOBNIJEG!"]
poruka = "Ustaj, izađi i glasaj! Ne dopusti da drugi u tvoje ime odlučuje. Ako ne znaš za koga - poništi listić. Ustaj BOSNO I HERCEGOVINO, DANAS NAROD SUDI!"
Region1 = Region(466,125,119,48)
Region2 = Region(949,126,58,49)
Region3 = Region(54,310,631,432)
Region4 = Region(531,368,169,72)
Region5 = Region(49,220,640,234)
Region6 = Region(571,258,114,48)
Region7 = Region(326,72,263,68)

def ponovoStartajOperu():
    os.system("echo 'hajpogodi' | killall -9 opera")
    wait(2)
    os.system("nohup opera %u --start-maximized > /dev/null 2>&1 &")    
    Region7.wait("1536172766535.png",10)
    wait(8)
    type(Key.F8)
    return pokreniNoviURL()

def prijava(usercommentsgroup):
    Region2.wait("1536057887146.png",10)
    click(Location(963,145))
    wait(2)
    type(Key.TAB)
    wait(0.5)
    def selectusergroup(usercommentsgroup):
        switcher = {
            1: users1,
            2: users2,
            3: users3,
            4: users4,
            5: users5,
            6: users6,
            7: users7,
            8: users8,
            9: users9,
            10: users10   
        }
        return switcher.get(usercommentsgroup, users2)
    user = random.choice(selectusergroup(usercommentsgroup))
    #user = random.choice(users2)        
    paste(user)
    wait(0.3)
    type(Key.TAB)
    wait(0.5)
    paste("budiiskrapromjene2018")
    wait(0.3)
    type(Key.ENTER) 
    wait(2)
    return user

def odjava():
    wait(5)
    Region2.hover(Location(970,140))
    wait(1)
    click(Location(864,338))
    wait(3)    

def randomClickOnKlixLandingPage():
    wait(3)
    for i in range(0,random.randint(0,5)):
        type(Key.PAGE_DOWN)
        wait(0.3)
    count = 0
    while (count < 10 and not Region6.exists("1536152158126.png")):   
        locx = int(Region3.x + Region3.w * random.random())
        locy = int(Region3.y + Region3.h * random.random())
        click(Location(locx, locy))
        count += 1
        wait(1)
    #click("1536078274346.png")    

def randomClickOnFilteredPage():
    for j in range(0,random.randint(2,50)):
        type(Key.TAB)
    type(Key.ENTER)    

def randomUrlFromFileSimple():
    f1 = open("klix/klix.txt")
    f2 = open("klix/klix_commented_urls.txt","a+")
    url = random.choice(f1.readlines())
    f2.write(url + "\r\n") 
    f1.close()   
    print url
    return url

def randomUrlFromFileComplex():
    f1 = open("klix/klix.txt")
    with f1:
        content = f1.readlines()
    content = [x.strip() for x in content] 
    
    sumOfComments = 0
    for line in content:
        elements  = line.split(';')
        sumOfComments += int(elements[1])
    print sumOfComments
    
    urlChosen = 0
    while (urlChosen == 0):
        line = random.choice(content)
        elements  = line.split(';')
        siteOfArticle = int(elements[0])
        commentsOnArticle = int(elements[1])
        url = elements[2]
        print line
        articleWeightFactor = (commentsOnArticle/math.log(commentsOnArticle+2,10))/siteOfArticle
        commentTreshold = random.randint(0,sumOfComments)/10
        if (articleWeightFactor > commentTreshold):
            urlChosen = 1
            #print 'komentiram'
    f1.close()
    print url, articleWeightFactor
    return url, articleWeightFactor

def pokreniNoviURL():
    type(Key.F8)
    url, articleWeightFactor = randomUrlFromFileComplex()
    paste(url)
    type(Key.ENTER)
    return url, articleWeightFactor

def pripremiPoruku(usercommentsgroup):
    f3 = open("klix/klix_comment_phrases" + str(usercommentsgroup) + ".txt")
    phrase = random.choice(f3.readlines())
    f3.close()    
    if usercommentsgroup in {1,2,5,6}: 
        for i in range(0,random.randint(2,10)):
            paste(unicode(flag,"utf8"))
        type(Key.ENTER)
    if usercommentsgroup in {3,4,7}:       
        if random.randint(0,2) == 0:
            for i in range(0,random.randint(2,5)):
                paste(unicode(random.choice(symbols),"utf8"))
            type(Key.ENTER)        
    #paste(unicode(phrase,"utf8")) 
    #type(Key.ENTER)
    if usercommentsgroup in {1,2,3,6,7}:     
        #paste(unicode(random.choice(poruka),"utf8"))        
        paste(unicode(poruka,"utf8"))
        type(Key.ENTER)
    if usercommentsgroup in {4,5,6,7}:         
        paste(unicode(random.choice(hashtags),"utf8"))            
    wait(2)    

def upisiPosaljiKomentar(url, articleWeightFactor, user, usercommentsgroup):
    f2 = open("klix/klix_commented_urls.txt","a+")
    Region5.wait("1536059379094.png",10)
    if (articleWeightFactor > 25) and random.randint(0,100) <= 25:
        for i in range (0,10):
            type(Key.DOWN)
            wait(1)
        click("1536355413811.png")
        wait(1)
        pripremiPoruku(usercommentsgroup)
        if  exists("1536355730671.png"):
            click("1536355730671.png")
            f2.write(url + " (articleWeightFactor = " + str(articleWeightFactor) + ") [u: " + user + " | t: " + datetime.now().strftime('%d.%m.%Y %H:%M:%S') + " | a: replied to comment]\r\n") 
            #print("nasao i kliknuo na odgovori")  
    else:
        Region5.click("1536059379094.png")
        wait(1)
        pripremiPoruku(usercommentsgroup)
        if Region5.exists("1536128416733.png"):
            Region5.click("1536128416733.png")
            f2.write(url + " (articleWeightFactor = " + str(articleWeightFactor) + ") [u: " + user + " | t: "+ datetime.now().strftime('%d.%m.%Y %H:%M:%S') + " | a: regular comment]\r\n")             
            #print("nasao i kliknuo na komentarisi")
        else:          
            click(Location(570,400))
            #print("kliknuo na lokaciju")
    f2.close()

url, articleWeightFactor = ponovoStartajOperu()
for i in range (0,brojKomentara):
    if (loopctl == True):    
        try:  
            Region1.wait("1536057616262.png",20)            
            usercommentsgroup = random.randint(1,7)
            user = prijava(usercommentsgroup)
        except FindFailed:
            print("vec prijavljen")
        try:
            #Region4.wait("1536059248737.png",10)
            #Region4.click("1536059248737.png")
            upisiPosaljiKomentar(url, articleWeightFactor, user, usercommentsgroup)
        except FindFailed:
            print("nije komentarisao")
        odjava()
        url, articleWeightFactor = pokreniNoviURL()
        wait(vrijemepauze)